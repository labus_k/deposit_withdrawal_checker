from exchanges.base import BaseWrapper

from .async_hotbit import hotbit


class HotbitWrapper(BaseWrapper):
    asset_info_url = 'https://www.hotbit.io/public/fee/withdraw'

    def __init__(self, exchange_api=hotbit()):
        super(HotbitWrapper, self).__init__(exchange_api)

    async def fetch_assets(self):
        """
        Fetch withdrawal/deposit availability info from API and combine it into dict

        :return: dict of withdrawal/deposit info with currency keys and
            {
                'CURRENCY_CODE': {
                    'withdrawal': True,
                    'deposit': False,
                }
            }
        """
        raw_info = await self.exchange_api.fetch(self.asset_info_url)
        try:
            asset_info = raw_info['Content']
        except KeyError as e:
            print(e)
            return {}

        parsed_assets = {}

        for coin, info in asset_info.items():
            common_currency = self.exchange_api.common_currency_code(coin)
            parsed_assets[common_currency] = {'deposit': info['is_deposit'], 'withdrawal': info['is_withdraw']}
        return parsed_assets
