import aiohttp

import ccxt.async_support as ccxt

from ccxt.base.errors import ExchangeError
from ccxt.base.errors import AuthenticationError
from ccxt.base.errors import InsufficientFunds
from ccxt.base.errors import InvalidOrder
from ccxt.base.errors import NotSupported
from ccxt.base.errors import DDoSProtection
from ccxt.base.errors import ExchangeNotAvailable
from ccxt.base.errors import InvalidNonce

class hotbit(ccxt.Exchange):

    def describe(self):
        return self.deep_extend(super(hotbit, self).describe(), {
            'id': 'hotbit',
            'name': 'Hotbit',
            'countries': ['CN'],
            'rateLimit': 2000,
            'userAgent': self.userAgents['chrome39'],
            'version': 'v1',
            'accounts': None,
            'accountsById': None,
            'hostname': 'api.hotbit.io/api',
            'has': {
                'CORS': False,
                'fetchDepositAddress': False,
                'fetchOHCLV': False,
                'fetchOpenOrders': True,
                'fetchClosedOrders': True,
                'fetchOrder': True,
                'fetchOrders': True,
                'fetchOrderBook': True,
                'fetchOrderBooks': False,
                'fetchTradingLimits': False,
                'withdraw': False,
                'fetchCurrencies': False,
            },
            'timeframes': {
                '1m': '60',
                '3m': '180',
                '5m': '300',
                '15m': '900',
                '30m': '1800',
            },
            'urls': {
                'logo': 'https://user-images.githubusercontent.com/1294454/42244210-c8c42e1e-7f1c-11e8-8710-a5fb63b165c4.jpg',
                'api': 'https://api.hotbit.io/api',
                'www': 'https://www.hotbit.io',
                'referral': 'https://www.hotbit.io/support?page=api',
                'doc': 'https://github.com/hotbitex/hotbit.io-api-docs',
                'fees': 'https://www.hotbit.io/support?page=fees',
            },
            'www': {
                'public/fee/withdraw'
            },
            'api': {
                'public': {
                    'get': [
                        'server.time',
                        'asset.list',
                        'order.book',
                        'order.depth',
                        'market.list',
                        'market.last',
                        'market.kline',
                        'market.status',
                        'market.status_today',
                        'market.status24h',
                        'market.summary',
                    ],
                    'post': [
                        'user_deals',  # by HL：api文档可能有问题
                    ]
                },
                'private': {
                    'get': [
                        'market.deals',
                    ],
                    'post': [
                        'balance.query',
                        'order.put_limit',
                        'order.cancel',
                        'order.deals',
                        'order.finished_detail',
                        'order.pending',
                        'order.finished',
                    ]
                }
            },
            'fees': {
                'trading': {
                    'tierBased': False,
                    'percentage': True,
                    'maker': 0.001,
                    'taker': 0.001,
                },
            },
            'limits': {
                'amount': {'min': 0.01, 'max': 100000},
            },
            'options': {
                'createMarketBuyOrderRequiresPrice': True,
                'limits': {
                    'ETH/USDT': {'amount': {'min': 0.001, 'max': 10000}},
                },
            },
            'exceptions': {
                '400': NotSupported,  # Bad Request
                '401': AuthenticationError,
                '405': NotSupported,
                '429': DDoSProtection,  # Too Many Requests, exceed api request limit
                '1002': ExchangeNotAvailable,  # System busy
                '1016': InsufficientFunds,
                '3008': InvalidOrder,
                '6004': InvalidNonce,
                '6005': AuthenticationError,  # Illegal API Signature
            },
            'commonCurrencies': {
                # 'DAG': 'DAGX',
                # 'PAI': 'PCHAIN',
            },
        })
