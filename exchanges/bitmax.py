import aiohttp
from exchanges.base import BaseWrapper


class BitmaxCurrencies:
    name = 'Bitmax'
    commonCurrencies = {}

    async def fetch_currencies(self):
        url = 'https://bitmax.io/api/v1/assets'
        async with aiohttp.ClientSession() as session:
            response = await session.get(url)
            currencies_dict = await response.json()
            result = {}
            for c in currencies_dict:
                coin = c['assetCode']
                status = not c['status']=='NoTransaction'
                result[coin] = status
            return result


class BitmaxWrapper(BaseWrapper):
    """
    Прямой инфы о депозитах-выводах я не нашел. Есть только параметр IsActive
        https://international.bittrex.com/status
        https://bittrex.com/api/v2.0/pub/currencies/GetWalletHealth
    """

    def __init__(self, exchange_api=BitmaxCurrencies()):
        super(BitmaxWrapper, self).__init__(exchange_api)

    async def fetch_assets(self):
        """
        Fetch withdrawal/deposit availability info from API and combine it into dict

        :return: dict of withdrawal/deposit info with currency keys and
            {
                'CURRENCY_CODE': {
                    'withdrawal': True,
                    'deposit': False,
                }
            }
        """
        raw_info = await self.exchange_api.fetch_currencies()

        parsed_assets = {}

        for coin_name, coin_data in raw_info.items():
            parsed_assets[coin_name] = {
                'deposit': coin_data,
                'withdrawal': coin_data}
        return parsed_assets
