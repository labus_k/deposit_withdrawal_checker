from ccxt.async_support import bittrex
from exchanges.base import BaseWrapper


class BittrexWrapper(BaseWrapper):
    """
    Прямой инфы о депозитах-выводах я не нашел. Есть только параметр IsActive
        https://international.bittrex.com/status
        https://bittrex.com/api/v2.0/pub/currencies/GetWalletHealth
    """

    def __init__(self, exchange_api=bittrex()):
        super(BittrexWrapper, self).__init__(exchange_api)

    async def fetch_assets(self):
        """
        Fetch withdrawal/deposit availability info from API and combine it into dict

        :return: dict of withdrawal/deposit info with currency keys and
            {
                'CURRENCY_CODE': {
                    'withdrawal': True,
                    'deposit': False,
                }
            }
        """
        raw_info = await self.exchange_api.fetch_currencies()

        parsed_assets = {}

        for coin_name, coin_data in raw_info.items():
            common_currency = self.exchange_api.common_currency_code(coin_name)
            parsed_assets[common_currency] = {
                'deposit': coin_data['info']['IsActive'],
                'withdrawal': coin_data['info']['IsActive']}
        return parsed_assets
