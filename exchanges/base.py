from cachetools import TTLCache

MINUTE_IN_SECONDS = 60
CACHE_MAXSIZE = 1


class BaseWrapper:
    def __init__(self, exchange_api_obj):
        self.exchange_api = exchange_api_obj
        self._cache = TTLCache(CACHE_MAXSIZE, MINUTE_IN_SECONDS)
        for exchange_code, common_code in self.exchange_api.commonCurrencies.items():
            self.exchange_api.commonCurrencies[exchange_code] = common_code.upper()

    def fetch_assets(self):
        """
        Fetch withdrawal/deposit availability info from API and combine it into dict

        :return: dict of withdrawal/deposit info with currency keys and
            {
                'CURRENCY_CODE': {
                    'withdrawal': True,
                    'deposit': False,
                }
            }
        """
        pass

    async def get_asset(self, coin):
        """
        Get asset detail (withdraw/deposit info).

        :param coin: str, currency code in uppercase
        :return: {'withdrawal': bool, 'deposit': bool}
        :raise: KeyError, if coin not in assets
        """
        try:
            assets = self._cache[self.exchange_api.name]
        except KeyError:
            assets = await self.fetch_assets()
            self._cache[self.exchange_api.name] = assets

        return assets[coin.upper()]
