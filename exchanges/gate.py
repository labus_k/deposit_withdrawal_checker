from ccxt.async_support import gateio
from exchanges.base import BaseWrapper


class GateWrapper(BaseWrapper):
    asset_info_url = 'https://data.gateio.co/api2/1/coininfo'

    def __init__(self, exchange_api=gateio()):
        super(GateWrapper, self).__init__(exchange_api)

    async def fetch_assets(self):
        """
        Fetch withdrawal/deposit availability info from API and combine it into dict

        :return: dict of withdrawal/deposit info with currency keys and
            {
                'CURRENCY_CODE': {
                    'withdrawal': True,
                    'deposit': False,
                }
            }
        """
        raw_info = await self.exchange_api.fetch(self.asset_info_url)
        try:
            asset_info = {}
            for coin_data in raw_info['coins']:
                asset_info.update(coin_data)
        except KeyError as e:
            print(e)
            return {}

        parsed_assets = {}

        for coin_name, coin_data in asset_info.items():
            common_currency = self.exchange_api.common_currency_code(coin_name)
            parsed_assets[common_currency] = {
                'deposit': not coin_data['deposit_disabled'],
                'withdrawal': not coin_data['withdraw_disabled']}
        return parsed_assets
