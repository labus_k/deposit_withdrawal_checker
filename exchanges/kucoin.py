from ccxt.async_support import kucoin
from exchanges.base import BaseWrapper


class KucoinWrapper(BaseWrapper):
    def __init__(self, exchange_api=kucoin()):
        super(KucoinWrapper, self).__init__(exchange_api)

    async def fetch_assets(self):
        """
        Fetch withdrawal/deposit availability info from API and combine it into dict

        :return: dict of withdrawal/deposit info with currency keys and
            {
                'CURRENCY_CODE': {
                    'withdrawal': True,
                    'deposit': False,
                }
            }
        """
        raw_info = await self.exchange_api.fetch_currencies()

        parsed_assets = {}

        for coin_name, coin_data in raw_info.items():
            try:
                common_currency = self.exchange_api.common_currency_code(coin_name)
                parsed_assets[common_currency] = {
                    'withdrawal': coin_data['info']['isWithdrawEnabled'],
                    'deposit': coin_data['info']['isDepositEnabled']
                }
            except KeyError:
                pass
        return parsed_assets
