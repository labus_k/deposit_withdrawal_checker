"""
File with available exchanges. Use it to setup available exchange and map names for it.
"""
from .bittrex import BittrexWrapper
from .gate import GateWrapper
from .kucoin import KucoinWrapper
from .hotbit import HotbitWrapper
from .hitbtc import HitBTCWrapper
from .huobi import HuobiWrapper
from .bitmax import BitmaxWrapper


exchange_dict = {
    'Hotbit': HotbitWrapper,
    'Gate.io': GateWrapper,
    'KuCoin': KucoinWrapper,
    'Bittrex': BittrexWrapper,
    'HitBTC': HitBTCWrapper,
    'HitBTC v2': HitBTCWrapper,
    'Huobi Pro': HuobiWrapper,
    'Bitmax': BitmaxWrapper,
}
