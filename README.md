faq
===============
урлы:

детальный по монете :
/assetInfo?exchange=kucoin&coin=ocn

список бирж :
/exchanges

для добавления новой - добавить в exchanges/__init__.py новый класс


aiohttp-devtool (for developing)
===============

Your new web app is ready to go!

To run your app you'll need to:

1. Activate a python 3.5 or 3.6 environment
2. Install the required packages with `pip install -r requirements.txt`
3. Make sure the app's settings are configured correctly (see `activate_settings.sh` and `app/settings.py`). You can also
 use environment variables to define sensitive settings, eg. DB connection variables
4. You can then run your app during development with `adev runserver .`


deployment-tool
===============

1. Create enviroment via ```python -m venv env```
2. Activate enviroment `source env/bin/activate`
3. Run app with

```bash
python -m app --port 55555
```

Another ways to deploy app can be found  [here](https://aiohttp.readthedocs.io/en/stable/deployment.html).

