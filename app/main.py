import asyncio
import argparse

from pathlib import Path

from aiohttp import web

from app.settings import Settings
from app.views import get_withdrawal_deposit_detail, get_exchanges, localhost_cors_enable_middleware
from exchanges import exchange_dict

THIS_DIR = Path(__file__).parent
BASE_DIR = THIS_DIR.parent
print(THIS_DIR)

parser = argparse.ArgumentParser(description="wirhdrawal/deposit checker server")
parser.add_argument('--port')

# TODO хранить все exchanges в общем поле 'exchange' или каждую exchange отдельно внутри app
# TODO сейчас применён второй вариант


def setup_exchanges(app: web.Application):
    available_exchanges = tuple(exchange_dict.keys())
    for name, exchange_wrapper in exchange_dict.items():
        app[name.lower()] = exchange_wrapper()
    app['exchanges'] = available_exchanges


def setup_routes(app: web.Application):
    app.router.add_get('/assetInfo', get_withdrawal_deposit_detail)
    app.router.add_get('/exchanges', get_exchanges)


def setup_signals(app: web.Application):
    app.on_cleanup.append(on_shutdown)


async def on_shutdown(app: web.Application):
    for exchange_name in app['exchanges']:
        exchange_wrapper = app[exchange_name.lower()]
        await exchange_wrapper.exchange_api.close()


async def create_app():
    app = web.Application(middlewares=[localhost_cors_enable_middleware])
    settings = Settings()
    app.update(
        name='aiohttp-devtool',
        settings=settings
    )
    setup_exchanges(app)
    setup_routes(app)
    setup_signals(app)
    return app


def main():
    args = parser.parse_args()
    loop = asyncio.get_event_loop()
    app = loop.run_until_complete(create_app())
    web.run_app(app, port=args.port)


if __name__ == "__main__":
    main()
