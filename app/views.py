from aiohttp import web

from exchanges.base import BaseWrapper

COIN_PARAM = 'coin'
EXCHANGE_PARAM = 'exchange'


async def get_withdrawal_deposit_detail(request: web.Request):
    query_params = request.query
    if COIN_PARAM not in query_params or EXCHANGE_PARAM not in query_params:
        return web.HTTPBadRequest()

    exchange_name = query_params[EXCHANGE_PARAM].lower()
    coin_code = query_params[COIN_PARAM]
    response_data = {'exchange': exchange_name, 'coin': coin_code}
    try:
        exchange_wrapper: BaseWrapper
        exchange_wrapper = request.app[exchange_name]
    except KeyError:
        response_data.update({'message': 'Exchange not found', 'success': False})
        return web.json_response(data=response_data)

    try:
        requested_data = await exchange_wrapper.get_asset(coin_code)
    except KeyError:
        response_data.update({'message': 'Coin not found', 'success': False})
        return web.json_response(data=response_data)

    response_data.update(requested_data)
    response_data['success'] = True
    return web.json_response(data=response_data)


async def get_exchanges(request: web.Request):
    try:
        exchanges = request.app['exchanges']
    except KeyError:
        return web.HTTPNotFound()
    return web.json_response(data={'exchanges': exchanges})


@web.middleware
async def localhost_cors_enable_middleware(request: web.Request, handler):
    try:
        resp = await handler(request)
    except web.HTTPException as e:
        resp = e
    resp.headers['Access-Control-Allow-Origin'] = '*'
    resp.headers['Access-Control-Allow-Methods'] = 'POST,GET'
    resp.headers['Access-Control-Allow-Headers'] = '*'
    return resp
